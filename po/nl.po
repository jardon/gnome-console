# Dutch translation for kgx.
# Copyright (C) 2020 kgx's COPYRIGHT HOLDER
# This file is distributed under the same license as the kgx package.
#
# Nathan Follens <nfollens@gnome.org>, 2020-2022.
# Hannie Dumoleyn <hannie@ubuntu-nl.org>, 2022.
# Philip Goto, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kgx master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/console/issues\n"
"POT-Creation-Date: 2022-08-31 00:11+0000\n"
"PO-Revision-Date: 2022-08-20 12:55+0200\n"
"Last-Translator: Philip Goto <philip.goto@gmail.com>\n"
"Language-Team: Dutch <gnome-nl-list@gnome.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1.1\n"

#: data/org.gnome.Console.desktop.in.in:3
#: data/org.gnome.Console.metainfo.xml.in.in:8 src/kgx-application.h:29
msgid "Console"
msgstr "Console"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Console.desktop.in.in:8
msgid "command;prompt;cmd;commandline;run;shell;terminal;kgx;kings cross;"
msgstr ""
"command;prompt;cmd;commandline;run;shell;terminal;kgx;kings cross;"
"opdrachtregel;opdrachtlijn;commandoregel;commandolijn;"

#: data/org.gnome.Console.desktop.in.in:21
msgid "New Window"
msgstr "Nieuw venster"

#: data/org.gnome.Console.desktop.in.in:27 src/kgx-window.ui:132
msgid "New Tab"
msgstr "Nieuw tabblad"

#: data/org.gnome.Console.metainfo.xml.in.in:9 src/kgx-application.c:361
msgid "Terminal Emulator"
msgstr "Terminal-emulator"

#: data/org.gnome.Console.metainfo.xml.in.in:11
msgid "A simple user-friendly terminal emulator for the GNOME desktop."
msgstr ""
"Een eenvoudige en gebruiksvriendelijke terminal-emulator voor het Gnome-"
"bureaublad."

#: data/org.gnome.Console.metainfo.xml.in.in:30
msgid "Terminal window"
msgstr "Terminalvenster"

#: data/org.gnome.Console.metainfo.xml.in.in:164 src/kgx-window.c:387
msgid "The GNOME Project"
msgstr "Het Gnome-project"

#: src/help-overlay.ui:12
msgctxt "shortcut window"
msgid "Application"
msgstr "Toepassing"

#: src/help-overlay.ui:16
msgctxt "shortcut window"
msgid "New Window"
msgstr "Nieuw venster"

#: src/help-overlay.ui:23
msgctxt "shortcut window"
msgid "Terminal"
msgstr "Terminal"

#: src/help-overlay.ui:27
msgctxt "shortcut window"
msgid "Find"
msgstr "Zoeken"

#: src/help-overlay.ui:33
msgctxt "shortcut window"
msgid "Copy"
msgstr "Kopiëren"

#: src/help-overlay.ui:39
msgctxt "shortcut window"
msgid "Paste"
msgstr "Plakken"

#: src/help-overlay.ui:46
msgctxt "shortcut window"
msgid "Tabs"
msgstr "Tabbladen"

#: src/help-overlay.ui:50
msgctxt "shortcut window"
msgid "New Tab"
msgstr "Nieuw tabblad"

#: src/help-overlay.ui:56
msgctxt "shortcut window"
msgid "Close Tab"
msgstr "Tabblad sluiten"

#: src/help-overlay.ui:62
msgctxt "shortcut window"
msgid "Next Tab"
msgstr "Volgend tabblad"

#: src/help-overlay.ui:68
msgctxt "shortcut window"
msgid "Previous Tab"
msgstr "Vorig tabblad"

#: src/help-overlay.ui:74
msgctxt "shortcut window"
msgid "Switch to Tab"
msgstr "Naar tabblad wisselen"

#: src/kgx-application.c:236
msgid "Cannot use both --command and positional parameters"
msgstr "Kan niet zowel --command als positionele parameters gebruiken"

#. Translators: The leading # is intentional, the initial %s is the
#. version of KGX itself, the latter format is the VTE version
#: src/kgx-application.c:333
#, c-format
msgid "# KGX %s using VTE %u.%u.%u %s\n"
msgstr "# KGX %s met VTE %u.%u.%u %s\n"

#. Translators: %s is the year range
#: src/kgx-application.c:345 src/kgx-window.c:382
#, c-format
msgid "© %s Zander Brown"
msgstr "© %s Zander Brown"

#: src/kgx-application.c:359 src/kgx-window.ui:44
msgid "King’s Cross"
msgstr "King’s Cross"

#: src/kgx-application.c:363
msgid "GPL 3.0 or later"
msgstr "GPL 3.0 of later"

#: src/kgx-application.c:428
msgid "Execute the argument to this option inside the terminal"
msgstr "Voer het argument bij deze optie in de terminal uit"

#: src/kgx-application.c:429
msgid "COMMAND"
msgstr "Opdracht"

#: src/kgx-application.c:437
msgid "Set the working directory"
msgstr "Stel de werkmap in"

#. Translators: Placeholder of for a given directory
#: src/kgx-application.c:439
msgid "DIRNAME"
msgstr "MAPNAAM"

#: src/kgx-application.c:447
msgid "Wait until the child exits (TODO)"
msgstr "Wachten tot het onderliggend proces afsluit (TODO)"

#: src/kgx-application.c:456
msgid "Set the initial window title"
msgstr "Stel de initiële venstertitel in"

#: src/kgx-application.c:457
msgid "TITLE"
msgstr "TITEL"

#: src/kgx-application.c:465
msgid "ADVANCED: Set the shell to launch"
msgstr "GEAVANCEERD: stel de uit te voeren shell in"

#: src/kgx-application.c:466
msgid "SHELL"
msgstr "SHELL"

#: src/kgx-application.c:474
msgid "ADVANCED: Set the scrollback length"
msgstr "GEAVANCEERD: stel de terugschuiflengte in"

#: src/kgx-application.c:475
msgid "LINES"
msgstr "REGELS"

#: src/kgx-application.c:484
msgid "[-e|-- COMMAND [ARGUMENT...]]"
msgstr "[-e|-- OPDRACHT [ARGUMENT...]]"

#: src/kgx-close-dialog.c:50
msgid "Close Window?"
msgstr "Venster sluiten?"

#: src/kgx-close-dialog.c:51
msgid ""
"Some commands are still running, closing this window will kill them and may "
"lead to unexpected outcomes"
msgstr ""
"Er worden nog enkele opdrachten uitgevoerd, dit venster sluiten zal ze "
"onmiddellijk beëindigen en kan tot onverwachte resultaten leiden"

#: src/kgx-close-dialog.c:56
msgid "Close Tab?"
msgstr "Tabblad sluiten?"

#: src/kgx-close-dialog.c:57
msgid ""
"Some commands are still running, closing this tab will kill them and may "
"lead to unexpected outcomes"
msgstr ""
"Er worden nog enkele opdrachten uitgevoerd, dit tabblad sluiten zal ze "
"onmiddellijk beëindigen en kan tot onverwachte resultaten leiden"

#: src/kgx-close-dialog.ui:18 src/kgx-terminal.c:877
msgid "_Cancel"
msgstr "Annu_leren"

#: src/kgx-close-dialog.ui:19
msgid "C_lose"
msgstr "S_luiten"

#: src/kgx-pages.ui:61
msgid "_Detach Tab"
msgstr "Tabbla_d losmaken"

#: src/kgx-pages.ui:67
msgid "_Close"
msgstr "_Sluiten"

#. translators: <b> </b> marks the text as bold, ensure they are
#. matched please!
#: src/kgx-simple-tab.c:168
#, c-format
msgid "<b>Read Only</b> — Command exited with code %i"
msgstr "<b>Alleen-lezen</b> — opdracht afgesloten met code %i"

#. translators: <b> </b> marks the text as bold, ensure they are
#. matched please!
#: src/kgx-simple-tab.c:177
msgid "<b>Read Only</b> — Command exited"
msgstr "<b>Alleen-lezen</b> — Opdracht afgesloten"

#. translators: <b> </b> marks the text as bold, ensure they are
#. matched please!
#: src/kgx-simple-tab.c:210
#, c-format
msgid "<b>Failed to start</b> — %s"
msgstr "<b>Starten mislukt</b> — %s"

#: src/kgx-tab.c:961
msgid "Command completed"
msgstr "Opdracht voltooid"

#: src/kgx-tab-button.ui:5
msgid "View Open Tabs"
msgstr "Geopende tabbladen bekijken"

#: src/kgx-terminal.c:870
msgid "You are pasting a command that runs as an administrator"
msgstr "U plakt een opdracht die uitgevoerd wordt met beheerdersrechten"

#. TRANSLATORS: %s is the command being pasted
#: src/kgx-terminal.c:874
#, c-format
msgid ""
"Make sure you know what the command does:\n"
"%s"
msgstr ""
"Zorg ervoor dat u zeker weet wat de opdracht doet:\n"
"%s"

#: src/kgx-terminal.c:878 src/menus.ui:24
msgid "_Paste"
msgstr "_Plakken"

#: src/kgx-theme-switcher.ui:16
msgid "Use System Colors"
msgstr "Systeemkleuren gebruiken"

#: src/kgx-theme-switcher.ui:44
msgid "Use Light Colors"
msgstr "Lichte kleuren gebruiken"

#: src/kgx-theme-switcher.ui:71
msgid "Use Dark Colors"
msgstr "Donkere kleuren gebruiken"

# Met het nieuwe over-venster ziet de lege regel tussen vertalers en meer info er vreemd uit, mij lijkt het beter om deze weg te laten.
# - Philip
#. Translators: Credit yourself here
#: src/kgx-window.c:393
msgid "translator-credits"
msgstr ""
"Nathan Follens <nfollens@gnome.org>\n"
"Philip Goto\n"
"Meer info over Gnome-NL https://nl.gnome.org"

#: src/kgx-window.ui:17
msgid "_New Window"
msgstr "_Nieuw venster"

#: src/kgx-window.ui:23
msgid "_Keyboard Shortcuts"
msgstr "Snel_toetsen"

#: src/kgx-window.ui:27
msgid "_About Console"
msgstr "_Over Console"

#: src/kgx-window.ui:52
msgid "Find in Terminal"
msgstr "Zoeken in terminal"

#: src/kgx-window.ui:60
msgid "Menu"
msgstr "Menu"

#: src/kgx-window.ui:78
msgid "Shrink Text"
msgstr "Tekst verkleinen"

#: src/kgx-window.ui:89
msgid "Reset Size"
msgstr "Tekstgrootte herstellen"

#: src/kgx-window.ui:106
msgid "Enlarge Text"
msgstr "Tekst vergroten"

#: src/menus.ui:7
msgid "_Open Link"
msgstr "Verwijzing _openen"

#: src/menus.ui:12
msgid "Copy _Link"
msgstr "Verwijzing _kopiëren"

#: src/menus.ui:19
msgid "_Copy"
msgstr "_Kopiëren"

#: src/menus.ui:29
msgid "_Select All"
msgstr "Alles _selecteren"

#: src/menus.ui:36
msgid "Show in _Files"
msgstr "Tonen in _Bestanden"

#~ msgid "Zander Brown"
#~ msgstr "Zander Brown"

#~ msgid "Console (Development)"
#~ msgstr "Console (ontwikkeling)"

#~ msgid "About this Program"
#~ msgstr "Over dit programma"

#~ msgid "Open in Co_nsole (Devel)"
#~ msgstr "Openen in Co_nsole (ontwikkeling)"

#~ msgid "Open in Co_nsole"
#~ msgstr "Openen in terminal"

#~ msgid "Start a terminal session for this location"
#~ msgstr "Terminalsessie starten vanaf deze locatie"

#~ msgid "Terminal"
#~ msgstr "Terminal"

#~ msgid "KGX Terminal Emulator"
#~ msgstr "KGX-terminal-emulator"

#~ msgid "_About King’s Cross"
#~ msgstr "_Over King’s Cross"

#~ msgid "Op_en in King’s Cross"
#~ msgstr "Op_enen in King’s Cross"

#~ msgid "command;prompt;cmd;commandline;run;shell;terminal;kgx;"
#~ msgstr ""
#~ "command;prompt;cmd;commandline;run;shell;terminal;kgx;opdrachtregel;"
#~ "opdrachtlijn;commandoregel;commandolijn;"

#~ msgid "child watcher"
#~ msgstr "Onderliggendproceswaarnemer"

#~ msgid "Copyright © %s Zander Brown"
#~ msgstr "Copyright © %s Zander Brown"

#~ msgid "Terminal (King’s Cross)"
#~ msgstr "Terminal (King’s Cross)"

#~ msgid "_OK"
#~ msgstr "_Oké"
